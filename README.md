# composant-react-brief1

Contexte du projet

Ce brief va te proposer de créer un premier composant, qui va représenter une ligne de contact dans une application de messagerie :

​

Crée un dossier "components" dans le dossier "src" de ton projet.

​

Dans le dossier "components", crée un nouveau composant fonctionnel "Contact.jsx".

​

Tu trouveras des informations sur la création d'un composant fonctionnel ici : https://beta.reactjs.org/learn/your-first-component

​

Ce composant doit recevoir en props trois informations :

    un nom
    un prénom
    un statut "en ligne" (qui peut être vrai ou faux)

​

Tu trouveras des informations sur les props ici : https://beta.reactjs.org/learn/passing-props-to-a-component

Le composant doit afficher le nom et le prénom de la personne, et le texte "online" si la personne est en ligne, et "offline" sinon.

​

Tu trouveras des informations sur l'affichage conditionnel ici : https://fr.reactjs.org/docs/conditional-rendering.html#inline-if-else-with-conditional-operator

​

Dans le composant App.jsx, appelle plusieurs fois ton composant Contact.jsx avec des valeurs différentes pour tester que tout fonctionne correctement.

​
Ce que tu dois faire :

​

    un composant fonctionnel Contact.jsx qui reçoit par des props des informations à afficher : un nom, un prénom, un statut "en ligne".
    en fonction des valeurs des props, afficher le JSX du contact avec le nom et le prénom de la personne, et le texte "online" si la personne est en ligne, et "offline" sinon
    appeler plusieurs fois le composant Contact.jsx dans App.jsx (avec des valeurs de props différentes)
    versionner le projet avec Gitlab.
