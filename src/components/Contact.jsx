import Statut from "./Statut";

// Création d'un composant fonctionnel Contact.jsx qui reçoit par des props des informations à afficher : un nom, un prénom, un statut "en ligne".
function Contact (props){
    return <div>
        <h1>{props.name} {props.surname}</h1>
        <Statut statut={props.statut}/>
            </div>;
}

export default Contact;



